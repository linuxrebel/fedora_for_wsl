  $pwd = (Resolve-Path .\).Path
  $user = $env:USERNAME
  $wsldir = "C:\Users\$user\AppData\Local\wsl\Fedora35"

cd $wsldir
.\Fedora35.exe  clean 
    if ( ( Test-Path -LiteralPath $wsldir\rootfs) -or ( Test-Path -LiteralPath $wsldir\ext4.vhdx)) {
        echo "User did not remove from WSL"
     }
    else { 
        cd ..\
        Remove-Item $wsldir -Force -Recurse
     }
cd $pwd
