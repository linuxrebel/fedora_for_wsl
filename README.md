# Fedora 35 for WSL
## as of 22 Oct 2021 there are 3 branches giving you 
  1. Fedora 35 (this branch)
  2. Fedora 34
  3. Fedora 33

Installs a working Fedora 35 into WSL 


![ScreenShot](images/fedora35.png)

## Requirements
Windows 10 Fall Creators Update x64 or later.
Windows Subsystem for Linux feature is enabled.

## Install Steps

  1. Download the zip file from the releases page (above)
  2. Unzip the contents into a directory in you home. 
  3. Open powershell (admin not needed)
  4. In powershell cd into the Fedora35 (it will be a longer name) directory you just created
  5. Once in the Fedora35xxxxx dir you should see the following files
     - Fedora35.exe
     - rootfs.tar.gz
     - setup.ps1
     - uninstall.ps1
  6. execute the setup.ps1 script by typing it's name at the prompt and hitting return. It will now do the following
     - create a dir in your home called wsl
     - create a dir in wsl called Fedora35
     - copy rootfs.tar.gz and Fedora35.exe into this new dir
     - run the Fedora35.exe to install Fedora into WSL
     - create a new user in Fedora that is the same as the user you are logged into Windows as (if you username is billyg then billyg will be your fedora username)
     - Set a password for your new Fedora User (it will ask you for a password, standard Linux rules apply)
     - Setup your user so that you can sudo 
     - create a link in your Linux home directory to your windows home directory. 
  7.  Profit.  You can now run "wsl -d Fedora35" and get into your new distro.  If you don't have any other wsl distro "wsl" will automatically choose Fedora35

## Uninstall

  1.  cd into the original folder you created during install
  2.  Run the uninstall.ps1 command
  3.  It will ask you to typ y to confirm
  4.  Fedora is now removed from your system.

## Other things you can do with Fedora35.exe
## How-to-Use(for Installed Instance)
#### exe Usage

Usage:
  1. (no args)
     - Open a new shell with your default settings.


  2. run (command line)
     - Run the given command line in that distro. Inherit current directory.


  3. runp (command line (includes windows path))
     - Run the path translated command line in that distro.


  4. config [setting [value]]
     - `--default-user <user>`: Set the default user for this distro to <user>
     - `--default-uid <uid>`: Set the default user uid for this distro to <uid>
     - `--append-path <on|off>`: Switch of Append Windows PATH to $PATH
     - `--mount-drive <on|off>`: Switch of Mount drives


  5. get [setting]
     - `--default-uid`: Get the default user uid in this distro
     - `--append-path`: Get on/off status of Append Windows PATH to $PATH
     - `--mount-drive`: Get on/off status of Mount drives
     - `--lxguid`: Get WSL GUID key for this distro


  6. backup [contents]
     - `--tgz`: Output backup.tar.gz to the current directory using tar command
     - `--reg`: Output settings registry file to the current directory
     

  7. help
      - Print this usage message.

The Fedora35.exe is a binary borrowed from the projects here. https://github.com/yuk7/wsldl
